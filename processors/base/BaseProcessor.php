<?php
	
	namespace app\processor;
	
	class BaseProcessor
	{
		protected $error;

		protected $output;

		public function getError()
        {
        	return $this->error;
        }

        public function getoutput()
        {
        	return $this->output;
        }

        public function returnException($e)
        {
                return "<p>Message: " . $e->getMessage() . "</p><p>File: " . $e->getFile() . "</p><p>Line: " . $e->getLine() . "</p>";
        }
	}