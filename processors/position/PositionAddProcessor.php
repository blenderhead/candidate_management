<?php
	
    namespace app\processor;

    use Yii;

    use app\models\Position;

	class PositionAddProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $position = new Position();
                $position->name = $data['name'];
                $position->save();

               	return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $this->returnException($e);
				return FALSE;
			}
		}
	}