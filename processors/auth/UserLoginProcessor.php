<?php
	
    namespace app\processor;

    use Yii;

    use app\models\User;

	class UserLoginProcessor extends BaseProcessor
	{
		public function process($data)
		{
			try
			{
                $email = $data['email'];
                $password = $data['password'];

                $user = User::findByEmail(['email' => $email]);

                if(!$user)
                {
                    $this->error = 'User not found.';
                    return false;
                }

                if(!Yii::$app->getSecurity()->validatePassword($password, $user->password))
                {
                    $this->error = 'Invalid username and or password.';
                    return false;
                }

                //$user = new User();
                //$user->email = $email;
                //$user->password = $password;

                Yii::$app->user->login($user);

               	return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $this->returnException($e);
				return FALSE;
			}
		}
	}