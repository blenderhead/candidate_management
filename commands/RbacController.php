<?php

    namespace app\commands;

    use Yii;
    use yii\console\Controller;

    class RbacController extends Controller
    {
        public function actionInit()
        {
            $auth = Yii::$app->authManager;

            $createData = $auth->createPermission('create_data');
            $createData->description = 'Create data';
            $auth->add($createData);

            $updateData = $auth->createPermission('update_data');
            $updateData->description = 'Update data';
            $auth->add($updateData);

            $readData = $auth->createPermission('read_data');
            $readData->description = 'Read data';
            $auth->add($readData);

            $deleteData = $auth->createPermission('delete_data');
            $deleteData->description = 'Delete data';
            $auth->add($deleteData);

            $admin = $auth->createRole('super_admin');
            $auth->add($admin);
            $auth->addChild($admin, $createData);
            $auth->addChild($admin, $readData);
            $auth->addChild($admin, $updateData);
            $auth->addChild($admin, $deleteData);

            $admin = $auth->createRole('site_admin');
            $auth->add($admin);
            $auth->addChild($admin, $createData);
            $auth->addChild($admin, $readData);
            $auth->addChild($admin, $updateData);
        }
    }