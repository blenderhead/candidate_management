<?php

    namespace app\commands;

    use Yii;
    use yii\console\Controller;

    use app\models\User;

    class UserSeedController extends Controller
    {
        public function actionCreate()
        {
            $user = new User();
            $user->name = 'Super Admin';
            $user->email = 'blwfish705@gmail.com';
            $user->password = Yii::$app->getSecurity()->generatePasswordHash('12345678');

            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole('super_admin');
            $user->role = $authorRole->name;
            $user->avatar = '';
            $user->save();
            
            $auth->assign($authorRole, $user->getId());
        }
    }