function showInlineError(data)
{
    if(data.data.errors.name != undefined)
    {
        $('.name').addClass("border-red-300 bg-red-50 color-red-800");
        $('.name_error').removeClass('hide');
    }
    else
    {
        $('.name').removeClass("border-red-300 bg-red-50 color-red-800");
        $('.name_error').addClass('hide');
    }
}