$(document).ready(function() {

	$('.set-currency').click(function(e) {

		e.preventDefault();

		var from = $(this).data('from');
		var to = $(this).data('to');

		$.ajax({
            type: 'GET',
            dataType: 'html',
            data: { 
                'from' : from,
                'to' : to,
            },
            url: baseUrl + '/backend/currency/set',
            success: function() {
                window.location.reload();
            },
        });

	});

});