<?php

use yii\db\Schema;
use yii\db\Migration;

class m151012_030500_create_users_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING,
            'password' => Schema::TYPE_STRING,
            'role' => Schema::TYPE_STRING,
            'avatar' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_TIMESTAMP,
            'updated_at' => Schema::TYPE_TIMESTAMP,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('users');
    }
}
