<?php

    namespace app\filters;

    use Yii;
    use yii\base\ActionFilter;

    class DisableCsrfFilter extends ActionFilter
    {
        public function beforeAction($action)
        {
            Yii::$app->controller->enableCsrfValidation = false;
            return parent::beforeAction($action);
        }
    }