<?php

    namespace app\filters;

    use Yii;
    use yii\base\ActionFilter;

    class AuthFilter extends ActionFilter
    {
        public function beforeAction($action)
        {
            if(Yii::$app->user->isGuest)
            {
                Yii::$app->response->redirect('/auth/login', 301)->send();
            }

            return parent::beforeAction($action);
        }
    }