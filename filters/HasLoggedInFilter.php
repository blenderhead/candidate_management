<?php

    namespace app\filters;

    use Yii;
    use yii\base\ActionFilter;

    class HasLoggedInFilter extends ActionFilter
    {
        public function beforeAction($action)
        {
            if(!Yii::$app->user->isGuest)
            {
                Yii::$app->response->redirect('/admin/dashboard', 301)->send();
            }

            return parent::beforeAction($action);
        }
    }