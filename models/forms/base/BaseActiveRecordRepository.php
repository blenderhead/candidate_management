<?php

    namespace app\models;

    use Yii;
    use yii\db\ActiveRecord;
    
    abstract class BaseActiveRecordRepository extends ActiveRecord
    {
        protected $error_messages;
        protected $data;

        abstract function getInput();

        abstract public function setFormData();

        public function runValidation()
        {
            $this->getInput();
            $this->load(Yii::$app->request->post());
            $this->setFormData();
            
            if($this->validate() == false)
            {
                $this->error_messages = $this->errors;
                return false;
            }

            return true;
        }

        public function getFormErrors()
        {
            return $this->error_messages;
        }

        public function getData()
        {
            return $this->data;
        }
    }
