<?php

    namespace app\models;

    use Yii;

    class UserLoginRepository extends BaseRepository
    {
        public $email;
        public $password;

        public function getInput()
        {
            $request = Yii::$app->request;
            $this->email = $request->post('email'); 
            $this->password = $request->post('password');
        }

        public function setFormData()
        {
            $this->data = array(
                'email' => $this->email,
                'password' => $this->password,
            );
        }
            
        public function rules()
        {
            return [
                // name, email, subject and body are required
                [['email', 'password'], 'required'],
                // email has to be a valid email address
                ['email', 'email'],
            ];
        }
    }
