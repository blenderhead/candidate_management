<?php

    namespace app\models;

    use Yii;

    class PositionAddRepository extends BaseActiveRecordRepository
    {
        public $name;

        public static function tableName()
        {
            return 'positions';
        }

        public function getInput()
        {
            $request = Yii::$app->request;
            $this->name = $request->post('name'); 
        }

        public function setFormData()
        {
            $this->data = array(
                'name' => $this->name,
            );
        }
            
        public function rules()
        {
            return [
                [['name'], 'required'],
                [['name'], 'unique', 'targetAttribute' => ['name']]
            ];
        }
    }
