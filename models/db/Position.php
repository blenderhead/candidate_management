<?php

    namespace app\models;

    use yii\db\ActiveRecord;

    class Position extends ActiveRecord
    {
        public static function tableName()
        {
            return 'positions';
        }
    }
