<?php

    namespace app\models;

    use yii\db\ActiveRecord;
    use yii\web\IdentityInterface;

    class User extends ActiveRecord implements IdentityInterface
    {
        public static function tableName()
        {
            return 'users';
        }

        public static function findByEmail($email)
        {
            return static::findOne(['email' => $email]);
        }

        public static function findIdentity($id)
        {
            return static::findOne($id);
        }

        public static function findIdentityByAccessToken($token, $type = null)
        {
            return static::findOne(['access_token' => $token]);
        }

        public function getId()
        {
            return $this->id;
        }

        public function getAuthKey()
        {
            return $this->auth_key;
        }

        public function validateAuthKey($authKey)
        {
            return $this->getAuthKey() === $authKey;
        }

        public function getRole()
        {
            return $this->role;
        }
    }
