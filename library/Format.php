<?php
    
    namespace app\util;

    use Yii;

	class Format
	{
		public static function apiResponse($error_code, $message, $data, $http_code)
        {
            $response = Yii::$app->response;
            $response->format = \yii\web\Response::FORMAT_JSON;
            Yii::$app->response->statusCode = $http_code;

            $response->data = array(
                'error' => $error_code,
                'message' => $message,
                'data' => $data
            );

            return $response;
        }

        public static function ioObject($properties_name, $data, $singular = FALSE)
        {
            $obj = new \stdClass();

            if($data)
            {
                if($singular)
                {
                    foreach($data as $datas)
                    {
                        $result = $datas;
                    }

                    $obj->{$properties_name} = $result;
                }
                else
                {
                    $obj->{$properties_name} = $data;
                }
                
            }
            else
            {
                if($singular)
                {
                    $obj->{$properties_name} = NULL;
                }
                else
                {
                    $obj->{$properties_name} = $data;
                }
            }

            return $obj;
        }
	}