<?php

    namespace app\controllers\auth;

    use Yii;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;

    use app\util\Message;
    use app\util\Format;

    use app\models\UserLoginRepository;
    use app\processor\UserLoginProcessor;

    class LogoutController extends Controller
    {
        public function actionIndex()
        {
            Yii::$app->user->logout();
            $this->redirect('/auth/login', 301)->send();
        }
    }