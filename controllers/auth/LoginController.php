<?php

    namespace app\controllers\auth;

    use Yii;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;

    use app\util\Message;
    use app\util\Format;

    use app\models\UserLoginRepository;
    use app\processor\UserLoginProcessor;

    class LoginController extends Controller
    {
        public function behaviors()
        {
            return [
                [
                    'class' => 'app\filters\HasLoggedInFilter',
                    'only' => ['index']
                ],
                [
                    'class' => 'app\filters\DisableCsrfFilter',
                ],
            ];
        }

        public function actionIndex()
        {
            // render view without layout
            return $this->renderPartial('//auth/login');
        }

        public function actionProcess()
        {
            $form_processor = new UserLoginRepository();

            if($form_processor->runValidation() == false)
            {
                $errors = $form_processor->getFormErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new UserLoginProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }
    }