<?php

	namespace app\controllers\admin;

    use Yii;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;

    use app\controllers\base\BaseAdminController;

    use app\util\Message;
    use app\util\Format;

    use app\models\Position;

    use app\models\PositionAddRepository;
    use app\processor\PositionAddProcessor;

    class PositionController extends BaseAdminController
    {
        public function behaviors()
        {
            return [
                [
                    'class' => 'app\filters\AuthFilter',
                ],
                [
                    'class' => 'app\filters\DisableCsrfFilter',
                ],
            ];
        }

    	public function actionIndex()
    	{
            $data['positions'] = Position::find()->indexBy('id')->all();
            
    		return $this->render('index', $data);
    	}

        public function actionAdd()
        {
            return $this->render('add');
        }

        public function actionSave()
        {
            $form_processor = new PositionAddRepository();

            if($form_processor->runValidation() == false)
            {
                $errors = $form_processor->getFormErrors();
                $message = new Message(9107, NULL);
                return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $errors), 200);
            }
            else
            {
                $data = $form_processor->getData();

                $processor = new PositionAddProcessor();

                if($processor->process($data) == FALSE)
                {
                    $message = new Message(1000, $processor->getError());
                    return Format::apiResponse($message->message_code, $message->message, Format::ioObject('errors', $message->data), 200);
                }

                return Format::apiResponse(0, Message::getSuccessMessage(), Format::ioObject('errors', array()), 200);
            }
        }

        public function actionEdit()
        {
            return $this->render('edit');
        }
    }