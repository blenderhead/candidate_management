<?php

	namespace app\controllers\admin;

    use Yii;
    use yii\web\Controller;
    use yii\web\NotFoundHttpException;

    use app\controllers\base\BaseAdminController;

    class DashboardController extends BaseAdminController
    {
        public function behaviors()
        {
            return [
                [
                    'class' => 'app\filters\AuthFilter',
                ],
            ];
        }

    	public function actionIndex()
    	{
    		return $this->render('index');
    	}
    }