<?php

	namespace app\controllers\base;

	use yii\web\Controller;

    class BaseAdminController extends Controller
    {
    	public $layout = 'admin';
    }