<?php 
	use yii\helpers\Url;
?>

<?php
	$this->title = 'Positions - Add';
?>

<?php $this->beginBlock('styles'); ?>

	<link href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
	<link href="/plugins/datepicker/css/datepicker.css" rel="stylesheet" />
    <link href="/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="/plugins/jcrop/jquery.Jcrop.css" rel="stylesheet" />

<?php $this->endBlock(); ?>

<?php $this->beginBlock('page_title'); ?>

	<?php echo 'Add Work Positions'; ?>

<?php $this->endBlock(); ?>

<?php $this->beginBlock('page_description'); ?>

	<?php echo 'Add work position.'; ?>

<?php $this->endBlock(); ?>

<div class="row">

	<div class="col-lg-12">

		<div class="panel">

			<div class="panel-body no-padding-left no-padding-right">
			
				<form class="form-horizontal" id="add-position" method="post" enctype="multipart/form-data" data-op="add">

					<div class="form-group no-margin-left no-margin-right">
						<label for="name" class="col-lg-2 control-label"><span class="required">*</span> <span class="required-info">required</span></label>
					</div>

					<div class="form-group no-margin-left no-margin-right margin-bottom-20 border-bottom-1 padding-bottom-20 border-grey-100">
						<label for="room_name" class="col-lg-2 control-label">Name <span class="required">*</span></label>
						<div class="col-lg-8">
							<input type="text" class="form-control name" id="name" placeholder="position name" name="name">
							<span class="name_error hide glyphicon glyphicon-remove form-control-feedback color-red-800 margin-right-15"></span>
						</div>
					</div>

					<div class="text-center margin-top-20 padding-top-20">
						<input type="hidden" id="op" name="op" value="add" />
						<button type="submit" class="btn btn-success btn-icon-left margin-right-5 save"><i class="fa fa-check"></i> Submit</button>
						<a href="<?php echo Url::toRoute(['admin/position']); ?>" type="button" class="btn btn-danger btn-icon-left margin-right-5 cancel"><i class="fa fa-times"></i> Cancel</a>
					</div>

				</form>
			
			</div>
		
		</div>
		
	</div>

</div><!-- /.row -->

<?php $this->beginBlock('scripts'); ?>
	<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

	<!-- datepicker -->
    <script src="/plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
	
	<!-- bootstrap select -->
    <script src="/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

    <script src="/plugins/jcrop/jquery.Jcrop.min.js" type="text/javascript"></script>
    
    <script src="/js/functions.js" type="text/javascript"></script>
    <script src="/js/errors/position_errors.js" type="text/javascript"></script>
	<script src="/js/classes/PositionManager.js" type="text/javascript"></script>

	<script>
		$(document).ready(function() {

			$(".room_description, .room_setup").wysihtml5({
				"lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": false, //Button which allows you to edit the generated HTML. Default false
                "link": false, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false //Button to change color of font  
			});

		});
	</script>
<?php $this->endBlock(); ?>