<?php 
	use yii\helpers\Url;
?>

<?php
	$this->title = 'Positions - Index';
?>

<?php $this->beginBlock('styles'); ?>

	<link href="/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" />

<?php $this->endBlock(); ?>

<?php $this->beginBlock('page_title'); ?>

	<?php echo 'Manage Work Positions'; ?>

<?php $this->endBlock(); ?>

<?php $this->beginBlock('page_description'); ?>

	<?php echo 'Add, edit or delete work positions.'; ?>

<?php $this->endBlock(); ?>

<div class="row">

    <div class="col-lg-12">
     
		<div class="panel no-border ">

            <div class="panel-title">
				<button onClick="window.location='<?php echo Url::toRoute(['admin/position/add']); ?>'" type="button" class="btn btn-success btn-icon-left margin-right-5"><i class="fa fa-bed"></i> Add Position</button>
				<button type="button" class="btn btn-warning btn-icon-left margin-right-5 delete-all"><i class="fa fa-minus-square-o"></i> Delete Selected</button>
			</div>

            <div class="panel-body padding-top-20 bg-white table_wrapper">

				<table id="example" class="table table-bordered table-hover">
			        <thead>
			            <tr>
			                <th>Name</th>
			                <th>Actions</th>
			            </tr>
			        </thead>
			 
			        <tfoot>
			            <tr>
			                <th>Name</th>
			                <th>Actions</th>
			            </tr>
			        </tfoot>
			 
			        <tbody>
			        	<?php foreach($positions as $position): ?>
				            <tr>
				                <td><?php echo $position->name; ?></td>
				                <td>
				                	<a href="<?php echo URL::toRoute('/admin/position/edit') . '?id=' . $position->id; ?>" type="button" class="btn btn-success btn-circle margin-right-5 edit" data-id="<?php echo $position->id; ?>"><i class="glyphicon glyphicon-pencil"></i></a>
					   				<button type="button" class="btn btn-warning btn-circle delete" data-id="<?php echo $position->id; ?>"><i class="glyphicon glyphicon-remove"></i></button>
				                </td>
				            </tr>
			        	<?php endforeach; ?>
			     	</tbody>
			    </table>

        	</div>

    	</div>

</div>

<?php $this->beginBlock('scripts'); ?>

	<script src="/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
	<script src="/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

	<script type="text/javascript">

		$(document).ready(function() {

			$('#example').DataTable();

		});

	</script>

	<script src="/js/classes/PositionManager.js" type="text/javascript"></script>

<?php $this->endBlock(); ?>