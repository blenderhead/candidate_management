<?php

    use yii\helpers\Html;
    use yii\helpers\Url;

    /* @var $this yii\web\View */
    /* @var $content string */
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
    
    <!-- BEGIN CORE FRAMEWORK -->
    <link href="/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
    <link href="/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/plugins/jqueryui/jquery-ui.css" rel="stylesheet" />
    <link href="/plugins/jqueryui/jquery-ui.theme.css" rel="stylesheet" />
    <!-- END CORE FRAMEWORK -->
    
    <!-- BEGIN PLUGIN STYLES -->
    <link href="/plugins/animate/animate.css}" rel="stylesheet" />
    <link href="/plugins/bootstrap-slider/css/slider.css" rel="stylesheet" />
    <link href="/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" />
    <link href="/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
    <link href="/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
    <link href="/plugins/waitme/waitMe.css" rel="stylesheet" />
    <!-- END PLUGIN STYLES -->
    
    <!-- BEGIN THEME STYLES -->
    <link href="/css/material.css" rel="stylesheet" />
    <link href="/css/style.css" rel="stylesheet" />
    <link href="/css/plugins.css" rel="stylesheet" />
    <link href="/css/helpers.css" rel="stylesheet" />
    <link href="/css/responsive.css" rel="stylesheet" />
    <link href="/css/custom.css" rel="stylesheet" />
    <!-- END THEME STYLES -->

    <script type="text/javascript">
        var baseUrl = "<?php echo Yii::$app->urlManager->createAbsoluteUrl(''); ?>";
    </script>

    <?php if (isset($this->blocks['styles'])): ?>
        <?= $this->blocks['styles'] ?>
    <?php endif; ?>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-leftside fixed-header">

    <?php $this->beginBody() ?>

    <!-- BEGIN HEADER -->
    <header>
        <a href="{{ URL::route('dashboard.index" class="logo"><i class="ion-ios-bolt"></i> <span>Ivory Dashboard</span></a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="navbar-btn sidebar-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <!--
            <div class="navbar-right">
                <form role="search" class="navbar-form pull-left" method="post" action="#">
                    <div class="btn-inline">
                        <input type="text" class="form-control padding-right-35" placeholder="Search..."/>
                        <button class="btn btn-link no-shadow bg-transparent no-padding padding-right-10" type="button"><i class="ion-search"></i></button>
                    </div>
                </form>
            </div>
            -->
        </nav>
    </header>
    <!-- END HEADER -->

    <div class="wrapper">

        <!-- BEGIN LEFTSIDE -->
        <div class="leftside">

            <div class="sidebar">
                <!-- BEGIN RPOFILE -->
                <div class="nav-profile">
                    <div class="thumb">

                        <img src="/img/avatar.jpg" class="img-circle" alt="" />

                        <!--<span class="label label-danger label-rounded">3</span>-->
                    </div>
                    <div class="info">
                        <a href="#">John Doe</a>
                        <ul class="tools list-inline">
                            <li><a href="#" data-toggle="tooltip" title="Settings"><i class="ion-gear-a"></i></a></li>
                        </ul>
                    </div>
                    <a href="<?php echo Url::toRoute(['auth/logout']); ?>" class="button"><i class="ion-log-out"></i></a>
                </div>
                <!-- END RPOFILE -->

                <!-- BEGIN NAV -->
                <div class="title">Navigation</div>
                
                <ul class="nav-sidebar">
                    <li class="<?php if($this->context->id == 'admin/dashboard') { echo 'active'; } ?>">
                        <a href="">
                            <i class="ion-home"></i> <span>Dashboard</span>
                        </a>
                    </li>

                    <li class="nav-dropdown">
                        <a href="#">
                            <i class="ion-ios-people"></i> <span>Candidate Management</span>
                            <i class="ion-chevron-right pull-right"></i>
                        </a>
                        <ul>
                            <li><a href="#">Candidates</a></li>
                            <li><a href="#">Advanced Search</a></li>
                        </ul>
                    </li>

                    <li class="nav-dropdown <?php if($this->context->id == 'admin/position') { echo 'active open'; } ?>">
                        <a href="#">
                            <i class="ion-soup-can"></i> <span>Data Management</span>
                            <i class="ion-chevron-right pull-right"></i>
                        </a>
                        <ul>
                            <li><a href="<?php echo Url::toRoute(['admin/position']); ?>">Position</a></li>
                            <li><a href="#">Grade</a></li>
                            <li><a href="#">Education</a></li>
                            <li><a href="#">Education Level</a></li>
                            <li><a href="#">Location</a></li>
                            <li><a href="#">Ethnicity</a></li>
                            <li><a href="#">Client</a></li>
                            <li><a href="#">Industry</a></li>
                            <li><a href="#">Status</a></li>
                        </ul>
                    </li>

                    <li class="nav-dropdown">
                        <a href="#">
                            <i class="ion-person"></i> <span>User Management</span>
                            <i class="ion-chevron-right pull-right"></i>
                        </a>
                        <ul>
                            <li><a href="">Users</a></li>
                            <li><a href="">Roles</a></li>
                            <li><a href="">Permissions</a></li>
                            <!--<li><a href="{{ URL::route('cms.testimoni.index">Testimonials</a></li>-->
                        </ul>
                    </li>

                    <li class="">
                        <a href="">
                            <i class="ion-document-text"></i><span>Proposal Management</span>
                        </a>
                    </li>

                    <li class="">
                        <a href=""><i class="ion-email"></i><span>Mailing Management</span></a>
                    </li>                    

                </ul>
                <!-- END NAV -->

            </div><!-- /.sidebar -->

        </div>
        <!-- END LEFTSIDE -->

        <!-- BEGIN RIGHTSIDE -->
        <div class="rightside">
            <!-- BEGIN PAGE HEADING -->
            <div class="page-head bg-grey-100">
                <h1 class="page-title">

                    <?php if (isset($this->blocks['page_title'])): ?>
                        <?= $this->blocks['page_title'] ?>
                    <?php endif; ?>
                    
                    <small>
                        <?php if (isset($this->blocks['page_description'])): ?>
                            <?= $this->blocks['page_description'] ?>
                        <?php endif; ?>
                    </small>
                </h1>
            </div>
            <!-- END PAGE HEADING -->

            <div class="container-fluid">
                <?= $content ?>
            </div><!-- /.container-fluid -->
        </div><!-- /.rightside -->
    </div><!-- /.wrapper -->
    <!-- END CONTENT -->
        
    <!-- BEGIN JAVASCRIPTS -->
    
    <!-- BEGIN CORE PLUGINS -->
    <script src="/plugins/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/plugins/bootstrap/js/holder.js"></script>
    <script src="/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
    <script src="/plugins/jqueryui/jquery-ui.js" type="text/javascript"></script>
    <script src="/js/core.js" type="text/javascript"></script>
    <script src="/js/currency.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    
    <!-- flot chart -->
    <!--
    <script src="{{ asset('assets/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/flot/jquery.flot.grow.js" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
    -->
    
    <!-- sparkline -->
    <script src="/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    
    <!-- bootstrap slider -->
    <script src="/plugins/bootstrap-slider/js/bootstrap-slider.js" type="text/javascript"></script>
    
    <!-- datepicker -->
    <script src="/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
    <script src="/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
    
    <!-- vectormap -->
    <script src="/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="/plugins/jquery-jvectormap/jquery-jvectormap-europe-merc-en.js" type="text/javascript"></script>
    
    <!-- counter -->
    <script src="/plugins/jquery-countTo/jquery.countTo.js" type="text/javascript"></script>
    
    <!-- rickshaw -->
    <script src="/plugins/rickshaw/vendor/d3.v3.js" type="text/javascript"></script>
    <script src="/plugins/rickshaw/rickshaw.min.js" type="text/javascript"></script>

    <!-- ion-rangeSlider -->
    <script src="/plugins/ion-rangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js"></script>
    
    <!-- knob -->
    <script src="/plugins/jquery-knob/jquery.knob.min.js" type="text/javascript"></script>

    <!-- bootstrap validator -->
    <script src="/plugins/bootstrapValidator/bootstrapValidator.min.js" type="text/javascript"></script>

    <!-- input mask -->
    <script src="/plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
    <script src="/plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
    <script src="/plugins/input-mask/jquery.inputmask.numeric.extensions.js" type="text/javascript"></script>
    
    <!-- switchery -->
    <script src="/plugins/switchery/switchery.min.js" type="text/javascript"></script>
    
    <!-- datepicker -->
    <script src="/plugins/datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    
    <!-- colorpicker -->
    <script src="/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js" type="text/javascript"></script>
    
    <!-- bootstrap select -->
    <script src="/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    
    <!-- iCheck -->
    <script src="/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

    <script src="/plugins/waitme/waitMe.js" type="text/javascript"></script>
    
    <!-- maniac -->
    <script src="/js/maniac.js" type="text/javascript"></script>

    <script src="/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

    <script src="/js/functions.js" type="text/javascript"></script>
    
    <!-- dashboard -->
    <script type="text/javascript">
        //maniac.loadchart();
        maniac.loadvectormap();
        maniac.loadbsslider();
        //maniac.loadrickshaw();
        maniac.loadcounter();
        maniac.loadprogress();
        maniac.loaddaterangepicker();
        showOpTooltip();
    </script> 

    <?php if (isset($this->blocks['scripts'])): ?>
        <?= $this->blocks['scripts'] ?>
    <?php endif; ?>

    <!-- END JAVASCRIPTS -->
    <?php $this->endBody() ?>

</body>
<!-- END BODY -->
</html>
<?php $this->endPage() ?>